#!/usr/bin/python

#importamos el modulo sys para obtener los argumentos del programa
import sys


def porcentaje_gc(secuencia):
    media= float(secuencia.count("C")+ (secuencia.count("G")))/len(secuencia)*100
    sumaA = secuencia.count("A")
    sumaT = secuencia.count("T")
    sumaG = secuencia.count("G")
    sumaC = secuencia.count("C")
    print ("A: %s T: %s G: %s C: %s" % (sumaA,sumaT,sumaG,sumaC))
    return
  
if __name__ == "__main__":
    # se ejecuta automaticamente
    porcentaje_gc(sys.argv[1])